﻿#include "header/PBgsound.h"
#include "header/PEvsound.h"

using namespace std;

int main()
{ 
    char c;
    while (c != 'x'){
        int a;
        ALubyte key, nkey;
        cout << "b - playback bgsound" << endl
             << "e - playback event sound" << endl
             << "x - exit" <<endl;
        cin>>key;

        if (key == 'b') {
            cout<<"playing background sound"<<endl;
            PBgsound *tutn = new PBgsound((char*)"bgsound.ogg");
            tutn->Custom();
            tutn->Play();
            cin>>nkey; //this for playback
            delete tutn;
        }
        else if (key == 'e') {

            ALfloat ResPos[3] = { 0.0, 0.0, 0.0 };
            ALfloat ResVel[3] = { 0.0, 0.0, 0.1 };

            ALfloat LisPos[] = { 0.0, 0.0, 0.0 };
            ALfloat LisVel[] = { 0.0, 0.0, 0.0 };
            ALfloat LisOri[] = { 0.0, -1.0, 0.0,  0.0, 1.0, 0.0 };

            cout<<"playing event sound"<<endl;
            PEvsound *tutn = new PEvsound((char*)"evsound.ogg");
            tutn->Custom(*ResPos, *ResVel, *LisPos, *LisVel, *LisOri);

            cout<<tutn->vorbisInfo;

            tutn->Play();
            cin>>nkey; //this for playback
            delete tutn;
        }
        //else if(key == 'v'){

            //ALfloat ResPos[3] = { 0.0, 0.0, 0.0 };
            //ALfloat ResVel[3] = { 0.0, 0.0, 0.1 };

            //ALfloat LisPos[] = { 0.0, 0.0, 0.0 };
            //ALfloat LisVel[] = { 0.0, 0.0, 0.0 };
            //ALfloat LisOri[] = { 0.0, -1.0, 0.0,  0.0, 1.0, 0.0 };

            //cout<<"playing voice sound"<<endl;
            //PVoicesound* voice = new PVoicesound((char*)"vsound.ogg");
            //voice->Custom(*ResPos, *ResVel, *LisPos, *LisVel, *LisOri);
            //voice->Play();
            //cin>>nkey;
            //delete voice;
        //}
        else if (key=='x') return 0;
        c=nkey;
    }
    return 0;
}
